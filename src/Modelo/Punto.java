/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 * Está clase permite el manejo de una coordenada cartesiana X,Y 
 * @author madarme
 */
public class Punto implements Comparable{
    
    private float x;
    private float y;

    /**
     * Constructor que crea un punto vacío
     */
    public Punto() {
    }

    
    /**
     * Constructor que crea dos puntos en el plano cartesiano
     * @param x coordenada en el punto X
     * @param y coordenada en el punto Y
     */
    public Punto(float x, float y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Constructor que a partir de una cadena crea los puntos x, y
     * Esto es el ejemplo de lanzar una Excepción
     * @param puntos una cadena con formato x,y. Ejemplo: 3,56 , sería el punto (X=3,Y=56)
     * @throws java.lang.Exception
     */
    public Punto(String puntos) throws Exception
    {
      if(puntos==null || puntos.isEmpty())  
      {
          throw new Exception("La cadena es vacía");
          
      }
      //else : no hay necesidad 
      String datosPuntos[]=puntos.split(",");
      if(datosPuntos.length!=2)
      {
          throw new Exception("La cadena no tiene las partes requeridas para crear el punto");
      }
      
      try{
        this.x=Float.parseFloat(datosPuntos[0]);
        this.y=Float.parseFloat(datosPuntos[1]);
      }catch(java.lang.NumberFormatException e)
      {
          throw new Exception("dato no válido, revise que su formato sea: num1, num2 "+e.getMessage());
      }
      // :)
     
    }
    
    
    /**
     *  Constructor con dos datos almacenados en un String
     * @param num1 representa el punto x
     * @param num2 representa el punto y
     */
    public Punto(String num1, String num2) throws RuntimeException
    {
    
      if(num1==null || num1.isEmpty())  
      {
          throw new RuntimeException("Dato vacío para el punto x");
          
      }
       if(num2==null || num2.isEmpty())  
      {
          throw new RuntimeException("Dato vacío para el punto y");
          
      }
      try{
        this.x=Float.parseFloat(num1);
        this.y=Float.parseFloat(num2);
      }catch(java.lang.NumberFormatException e)
      {
          throw new RuntimeException("dato no válido, puntos no contienen datos float "+e.getMessage());
      } 
       
        
    }
    
    
    
    /**
     * Obtiene el valor del punto en el eje X
     * @return un entero con el valor del punto X
     */
    public float getX() {
        return x;
    }

    /**
     * Actualza el valor del punto en el eje X
     * @param x un entero con el nuevo valor del punto X
     */
    public void setX(float x) {
        this.x = x;
    }

    
     /**
     * Obtiene el valor del punto en el eje Y
     * @return un entero con el valor del punto Y
     */
    public float getY() {
        return y;
    }

    /**
     * Actualza el valor del punto en el eje Y
     * @param y un entero con el nuevo valor del punto Y
     */
    public void setY(float y) {
        this.y = y;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 47 * hash + Float.floatToIntBits(this.x);
        hash = 47 * hash + Float.floatToIntBits(this.y);
        return hash;
    }

    /**
     *  Compara si dos puntos están en el mismo eje y valor
     * @param obj un objeto de la clase Punto 
     * @return True si los objetos son iguales o false en caso contrario
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Punto other = (Punto) obj;
        
        return ((this.x==other.getX())&& (this.y==other.getY()));
        
    }

    /**
     * Obtiene el valor del punto
     * @return un String con el valor de los puntos en el plano cartesiano
     */
    @Override
    public String toString() {
        return "("+this.x+","+this.y+")";
    }

    @Override
    public int compareTo(Object obj) {
        if (this == obj) {
            return 0;
        }
        final Punto other = (Punto) obj;
            
        if(this.x == other.getX() && this.y == other.getY())
            return 0;
        
        if(this.x == other.getX() && this.y != other.getY()){ // (5,2)(5,3) en este ejemplo return -1
            if(this.y > other.y)
                return 1;
            else return -1;
        }
        if(this.x > other.getX())
            return 1;
        else return -1;

    }
    
    
    
    
    
}
